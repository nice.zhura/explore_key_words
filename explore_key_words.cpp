#include "test_runner.h"

#include <stack>
#include <future>
using namespace std;


// STATS STRUCT
//----------------------------------------------------------------------
struct Stats {
  map<string, int> word_frequences;

  void operator += (const Stats& other);
};

void Stats::operator+=(const Stats &other) {
    for (const auto& item : other.word_frequences) {
        word_frequences[item.first] += item.second;
    }
}
//----------------------------------------------------------------------

// PARSE STREAM INTO STACK OF STRINGS
//----------------------------------------------------------------------
stack<string> ParseStream(istream& input) {
    stack<string> string_stack;
    for (string line; getline(input, line);) {
        string_stack.push(move(line));
    }
    return string_stack;
}
//----------------------------------------------------------------------

// EXPLORE SINGLE STRING
//----------------------------------------------------------------------
Stats ExploreLine(const set<string>& key_words, const string& line) {
    Stats result_stats;

    stringstream line_stream(line);
    for (string word; line_stream >> word;) {
        if (key_words.count(word)) {
            ++result_stats.word_frequences[move(word)];
        }
    }

    return result_stats;
}
//----------------------------------------------------------------------

// EXPLORE MULTI THREAD
//----------------------------------------------------------------------
Stats ExploreKeyWords(const set<string>& key_words, istream& input) {
    // push lines into stack
    stack<string> string_stack = ParseStream(input);

    // create stack of input strings per thread
    stack<string> thread_strings;

    // create stack of <future> stats
    stack<future<Stats>> stats;

    // max value for input strings per thread
    const size_t INPUT_STRINGS_PER_THREAD = 5000;

    // get stats from input strings threaded
    while (!string_stack.empty()) {

        thread_strings.push(string());

        int counter = 0;
        while (counter < INPUT_STRINGS_PER_THREAD) {
            if (string_stack.empty()) { break; }
            thread_strings.top() += (" " + string_stack.top());
            string_stack.pop();
            ++counter;
        }
        if (counter) {
            stats.push(async(ExploreLine, ref(key_words), ref(thread_strings.top())));
        }
    }

    // get results from threads and clear stats stack
    Stats result_stats;
    while (!stats.empty()) {
        result_stats += stats.top().get();
        stats.pop();
    }

    // clear thread_strings stack
    while (!thread_strings.empty()) {
        thread_strings.pop();
    }

    // return resulting stats
    return result_stats;
}
//----------------------------------------------------------------------

// TESTS
//----------------------------------------------------------------------
void TestBasic() {
  const set<string> key_words = {"yangle", "rocks", "sucks", "all"};

  stringstream ss;
  ss << "this new yangle service really rocks\n";
  ss << "It sucks when yangle isn't available\n";
  ss << "10 reasons why yangle is the best IT company\n";
  ss << "yangle rocks others suck\n";
  ss << "Goondex really sucks, but yangle rocks. Use yangle\n";

  const auto stats = ExploreKeyWords(key_words, ss);
  const map<string, int> expected = {
    {"yangle", 6},
    {"rocks", 2},
    {"sucks", 1}
  };
  ASSERT_EQUAL(stats.word_frequences, expected);
}
//----------------------------------------------------------------------

// MAIN
//----------------------------------------------------------------------
int main() {
    TestRunner tr;
    RUN_TEST(tr, TestBasic);
}
//----------------------------------------------------------------------
